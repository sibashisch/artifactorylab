package oal.oracle.apps.csa.service.api.test;

import lombok.RequiredArgsConstructor;
import lombok.Getter;

@RequiredArgsConstructor
@Getter
class ArtifactTest {
	
	private final int value;
	
	public int sum (int x) {
		return this.value + x;
	}
	
}